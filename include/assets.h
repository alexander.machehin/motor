#ifndef ASSETS
#define ASSETS

#include "mains.h"
#include "motor.h"

class Asset : public Filesystem
{
    public:
        SDL_Texture *AssetOpt;
        SDL_Surface *TempSurface;
        SDL_Surface *TempBackground;
        SDL_Rect SrcR;
        SDL_Rect DestR;
        SDL_Rect *SolidBackground;
        SDL_Rect Clips[];
        SDL_RWops *RW;
        const std::string File;
        std::string ImageManip;
        int AssetID;
        int Velocity;
        int AssetHeight;
        int AssetWidth;
        int AssetXOfst;
        int AssetYOfst;
        int ClipHeight;
        int ClipWidth;
        int Frames;
        Uint32 SolidColor;
    public:
        Asset();
        ~Asset();
        void DestTexture();
        void DrawAsset(std::string ImageManip);
        int LoadAsset(int AssetID, const std::string File);
        int SetRects(int AssetHeight, int AssetWidth, int AssetXOfst, int AssetYOfst);
        int AssetStretch(int AssetHeight, int AssetWidth);
        int SetClip(int ClipHeight, int ClipWidth, int Frames);
        int SetBackColor(Uint32 SolidColor);
};

Asset::Asset()
{

}

Asset::~Asset()
{
    DestTexture();
    Filesystem::CloseFile();
}

// Loads asset from archive
// @AssetID ID number for Asset
// @File File to be opened, fed to PhysFS and SDL
int Asset::LoadAsset(int AssetID, const std::string File)
{
    if (FileCheck(File.c_str()) == 0) {
        OpenFile(File.c_str());
        RW = SDL_RWFromMem(Data, Size);
        TempSurface = IMG_Load_RW(RW,0);
        AssetOpt = SDL_CreateTextureFromSurface(GetRenderer(), TempSurface);
        SDL_FreeRW(RW);
    }

    if (&TempSurface != NULL) {
        SDL_FreeSurface(TempSurface);
        std::cout << "AssetID: " << AssetID << "\tFreed surface\n";
    }
    else if (&TempSurface == NULL) {
        std::cout << "AssetID: " << AssetID << "\tNo surface to free\n";
        SDLErrorPrint();
        return -1;
    }

    if (&AssetOpt == NULL) {
        std::cout << "AssetID: " << AssetID << "\tCould not load Asset\n";
        SDLErrorPrint();
        return -2;
    }
    else if (&AssetOpt != NULL) {
        std::cout << "AssetID: " << AssetID
            << "\tAsset " << File << " loaded.\n";
        return 0;
    }
}

// Finish me
int Asset::SetClip(int ClipHeight, int ClipWidth, int Frames)
{
    Frames = Frames - 1;
    Clips[Frames];
}

// May be out of scope for this, need fix pls
int Asset::SetBackColor(Uint32 SolidColor)
{
    SDL_FillRect(TempBackground, SolidBackground, SolidColor);
}

// Asset dimensions
// @AssetHight @AssetWidth sets height and width
// @AssetXOfst @AssetYOfst sets Asset X and Y offsets on screen
int Asset::SetRects(int AssetHeight, int AssetWidth, int AssetXOfst, int AssetYOfst)
{
    SrcR.x = 0;
    SrcR.y = 0;
    SrcR.h = AssetHeight;
    SrcR.w = AssetWidth;
    DestR.x = AssetXOfst;
    DestR.y = AssetYOfst;
    DestR.h = AssetHeight;
    DestR.w = AssetWidth;
    return 0;
}

// Destroys texture created by LoadAsset
void Asset::DestTexture()
{
    if (AssetOpt != NULL) {
        SDL_DestroyTexture(AssetOpt);
        std::cout << "Texture " << File << " destroyed\n";
    }
    else if (AssetOpt == NULL) {
        std::cout << "No texture to destroy\n";
        SDLErrorPrint();
    }
}


void Asset::DrawAsset(std::string ImageManip)
{
    if (ImageManip == "Stretch" || ImageManip == "stretch") {
        SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, NULL);
    }
    else if (ImageManip == "" || ImageManip == " ") {
        SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, &DestR);
    }
    SDL_RenderPresent(GetRenderer());
}

#endif
