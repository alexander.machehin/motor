// TODO

#ifndef INIT
#define INIT

#include "mains.h"
#include "motor.h"
#include "filesystem.h"

class Init : public Filesystem
{
    public:
        Init();
        ~Init();
        bool InitSDL();
        void DeInitSDL();
        void DeInitSDLIMG();
        void DeInitSDLTTF();
        void DeInitPhysFS();
        int InitSDLIMG();
        int InitSDLTTF();
        int InitPhysFS();
};

Init::Init()
{
    InitSDL();
    InitSDLIMG();
    InitSDLTTF();
    InitPhysFS();
}

Init::~Init()
{
    DeInitSDL();
    DeInitSDLIMG();
    DeInitSDLTTF();
    DeInitPhysFS();
}

bool Init::InitSDL()
{
    if (SDL_Init(SDL_INIT_EVERYTHING == -1)) {
        return false;
    }

    std::cout << "System init\n";
    return true;

}

void Init::DeInitSDL()
{
    std::cout << "System deinit\n";
    SDL_Quit();
}

int Init::InitSDLIMG()
{
    IMG_Init(IMG_INIT_PNG);
    std::cout << "SDL_IMG is init\n";
}

void Init::DeInitSDLIMG()
{
    std::cout << "SDL_IMG is deinit\n";
    IMG_Quit();
}

int Init::InitPhysFS()
{
    PHYSFS_init(NULL);
    std::cout << "PhysFS is init\n";
    if (PHYSFS_init(NULL) != 0) {
        std::cout << "PhysFS failed to init\n";
    }
}

void Init::DeInitPhysFS()
{
    std::cout << "PhysFS is deinit\n";
    PHYSFS_deinit();
}

int Init::InitSDLTTF()
{
    TTF_Init();
    if (TTF_Init() == -1) {
        std::cout << "SDL_TTF failed to init\n";
    }
    else if (TTF_Init() == 0) {
        std::cout << "SDL_TTF is init\n";
    }
}

void Init::DeInitSDLTTF()
{
    TTF_Quit();
    std::cout << "SDL_TTF is deinit\n";
}

#endif
