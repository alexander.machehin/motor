#ifndef PARSER
#define PARSER

#include "mains.h"

class YML
{
    public:
        FILE oFile;
        yaml_parser_t Parser;
        yaml_token_t yEvent;
        const std::string YMLFile;

        YML();
        ~YML();
        int SetParseFile(const std::string YMLFile);
        int SetParser();
        int SetToken();
        void DestParser();
        bool ParseInit();
};

YML::YML()
{

}

YML::~YML()
{
    DestParser();
}

int YML::SetParseFile(const std::string YMLFile)
{
    FILE *oFile = fopen(YMLFile.c_str(), "r");
    SetParser();
    SetToken();
    if (!yaml_parser_initialize(&Parser))
        std::cout << "Failed to init parser\n";
    else if (yaml_parser_initialize != NULL)
        std::cout << "Parser init\n";
    if (oFile == NULL)
        std::cout << "Failed to open file:\t" << YMLFile << "\n";
    else if (oFile != NULL) {
        std::cout << "File " << YMLFile << " opened successfully\n";
        yaml_parser_set_input_file(&Parser, oFile);
    }
}

int YML::SetParser()
{
    yaml_parser_t Parser;
}

int YML::SetToken()
{
    yaml_token_t Token;
}

void YML::DestParser()
{
    yaml_parser_delete(&Parser);
    std::cout << "Parser destroyed\n";
}

#endif
