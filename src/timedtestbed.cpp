#include "../include/mains.h"
#include "../include/motor.h"

int main (int args, char *argc[])
{
    WriteToLog();
    GlobalDebug(1);
    bool quit = false;

    Init SYSINIT;
    Filesystem TestSprites;
    TestSprites.SetArchive("res/testsprites.zip");
    Filesystem TestFail;
    TestFail.SetArchive("res/none.zip");
    TestFail.FileCheck("none.png");
    TestFail.OpenFile("none.png");

    CreateWindow(1280, 720);
    SetWindowIcon("res/background.png");

    Player TestPlayer;
    Entity TestEnt1;
    Entity TestEnt2;
    Asset Background;

    Background.LoadAsset(0,"background.png");
    Background.SetRects(20,20,0,0);
    Background.DrawAsset("stretch");

    TestPlayer.LoadAsset(1,"testplayersprite_dleft.png");
    TestPlayer.SetRects(75,45,0,0);
    TestPlayer.HandleInput();
    TestPlayer.ShowMove();
    TestPlayer.DrawAsset("");

    TestEnt1.LoadAsset(2,"testplayersprite_dright.png");
    TestEnt1.SetRects(75,45,100,0);
    TestEnt1.DrawAsset("");

    TestEnt2.LoadAsset(3,"testplayersprite_down.png");
    TestEnt2.SetRects(75,45,100,100);
    TestEnt2.DrawAsset("");

    Text Font;
    Font.SetFont(30,"res/test.ttf");
    Font.LoadText("Hello world",255,255,255);
    Font.DrawText();

    YML Config;
    Config.SetParseFile("public.yaml");

    GlobalTimer fps;

    SDL_Delay(3000);
    DestRenderer();
    DestWindow();

    return 0;
}
